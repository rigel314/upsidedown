FROM archlinux

RUN pacman -Sy --noconfirm squid apache wget curl sslstrip imagemagick patch && \
    rm /var/lib/pacman/sync/*.db && \
    mkdir -p /etc/systemd/system/sysinit.target.wants/ && \
    ln -sf /usr/lib/systemd/system/squid.service /etc/systemd/system/sysinit.target.wants/ && \
    ln -sf /usr/lib/systemd/system/httpd.service /etc/systemd/system/sysinit.target.wants/

COPY service/* /usr/lib/systemd/system/

COPY bin/* /usr/local/bin/

COPY conf/ /conf/
RUN for i in /conf/*.patch; do patch -u -i "$i" -p0; done && rm -rf conf

RUN mkdir /srv/http/images && chown proxy:proxy /srv/http/images && chmod 2775 /srv/http/images && usermod -aG proxy http

RUN ln -sf /usr/lib/systemd/system/sslstrip.service /etc/systemd/system/sysinit.target.wants/

ENTRYPOINT ["/lib/systemd/systemd", "--unit=sysinit.target"]
