upsidedown
======

# About
I wanted to implement the upside down ternet thing from many years ago.

# Running
```bash
docker run --name=upsidedown -e container=docker \
    --tmpfs /run --tmpfs /tmp --tmpfs /logs \
    -v /sys/fs/cgroup:/sys/fs/cgroup:ro \
    --net=host \
    -d \
    registry.gitlab.com/rigel314/upsidedown:latest
```
